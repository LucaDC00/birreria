<x-layout>
    <div class="nav-space"></div>
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-3">
                <form method="POST" action="{{route('login')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail2" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" name="email"> 
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

</x-layout>