<x-layout>
  <div class="nav-space"></div>
  <div class="container">
    <div class="row">
      <form action="{{route('register')}}" method="POST">
          @csrf
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail2" class="form-label">Nome</label>
            <input type="name" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" name="name">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" name="password">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword2" class="form-label">Conferma password</label>
            <input type="password" class="form-control" id="exampleInputPassword2" name="password_confirmation">
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>

</x-layout>